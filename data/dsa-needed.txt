A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
cacti
--
dnsdist (jmm)
--
dnsmasq
--
frr
  Tobias Frost (tobi) proposed to work on preparing an update, but discussion
  with Debian maintainer for status on bullseye + updates
--
git
  Maintainer is queried to prepare an update
--
gpac/oldstable
--
h2o (jmm)
--
libarchive
--
libreswan (jmm)
  Maintainer prepared bookworm-security update, but needs work on bullseye-security backports
--
linux (carnil)
  Wait until more issues have piled up, though try to regulary rebase for point
  releases to more recent v5.10.y and 6.1.y versions
--
nbconvert/oldstable
  Guilhem Moulin proposed an update ready for review
--
nodejs (aron)
--
opennds/stable
--
php-cas/oldstable
--
php-horde-mime-viewer/oldstable
--
php-horde-turba/oldstable
--
pillow (jmm)
--
pymatgen/stable
--
python-aiohttp
--
python-asyncssh
--
ring/oldstable
  might make sense to rebase to current version
--
roundcube
--
ruby2.7/oldstable
  Utkarsh Gupta offered help in preparing updates
--
ruby-nokogiri/oldstable
--
ruby-rails-html-sanitizer/oldstable
--
ruby-sinatra/oldstable
  Maintainer posted packaging repository link with proposed changes for review
--
ruby-tzinfo/oldstable
--
squid
--
tinyproxy (jmm)
--
zabbix
--
