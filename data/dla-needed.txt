An LTS security update is needed for the following source packages.

To add a new entry, please coordinate with this week's Front-Desk
person, and use the 'package-operations' LTS tool.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

When checking what packages to work on, use:
$ ./find-work
from the LTS admin repository, to sort packages by priority and
display important notes about the package (special attention, VCS,
testing procedures, programming language, etc.).

To work on a package, simply add your name behind it. To learn more about how
this list is updated have a look at
https://lts-team.pages.debian.net/wiki/Development.html#triage-new-security-issues

To make it easier to see the entire history of an update, please append notes
rather than remove/replace existing ones.

--
ansible
  NOTE: 20231202: Added by Front-Desk (Beuc)
  NOTE: 20231202: Supported package, but there's a CVE backlog, and no updates since 2021
  NOTE: 20231202: (neither in LTS nor in stable/oldstable), so this is an opportunity to
  NOTE: 20231202: assess/fix the situation.
  NOTE: 20231217: Begin to triage CVEs (rouca)
  NOTE: 20231217: Triaging done a few mail send upstream for claryfication purposes (rouca)
  NOTE: 20231228: Made a partial release DLA-3695-1 (rouca), waiting for lee
  NOTE: 20240501: Update for bookworm-proposed-update: #1070193 (lee)
--
atril (utkarsh)
  NOTE: 20240121: Added by Front-Desk (apo)
  NOTE: 20240121: Decide whether it makes sense to disable comic feature or use libarchive instead.
  NOTE: 20240319: package ready at: https://people.debian.org/~utkarsh/lts/atril/
  NOTE: 20240319: needs testing as the backport was a bit sensitive. (utkarsh)
  NOTE: 20240603: have an update proposed, waiting on some feedback for lts-coordinator. (utkarsh)
--
bind9
  NOTE: 20240518: Added by Front-Desk (utkarsh)
  NOTE: 20240531: Lengthy discussion here <https://lists.debian.org/debian-lts/2024/03/msg00064.html> (dleidert)
--
cacti
  NOTE: 20240519: Added by Front-Desk (utkarsh)
  NOTE: 20240519: whilst most of them are moderate severity SQL injections
  NOTE: 20240519: issues, but there's also XML and RCE with higher severity.
  NOTE: 20240519: I'd have postponed them but let's fix it before buster
  NOTE: 20240519: goes EOL. (utkarsh)
--
dcmtk (Adrian Bunk)
  NOTE: 20240428: Added by Front-Desk (ta)
--
dlt-daemon (utkarsh)
  NOTE: 20240519: Added by Front-Desk (utkarsh)
  NOTE: 20240519: 1 buffer-overflow, 1 memory leak, and 2 crashes. I think we
  NOTE: 20240519: can postpone these but I am in split mind. Will take it myself
  NOTE: 20240519: and decide further. (utkarsh)
  NOTE: 20240603: have an update proposed, waiting on some feedback for lts-coordinator. (utkarsh)
--
dnsmasq (dleidert)
  NOTE: 20240303: Added by Front-Desk (apo)
  NOTE: 20240325: Automatically unassigned (lamby)
  NOTE: 20240327: Claimed by lamby, started thread on deblts-team. (lamby)
  NOTE: 20240403: Re-assigned back to dleidert; see thread on deblts-team list. (lamby)
--
docker.io
  NOTE: 20230303: Added by Front-Desk (Beuc)
  NOTE: 20230303: Follow fixes from bullseye 11.2 (3 CVEs) (Beuc/front-desk)
  NOTE: 20230424: Is in preparation. (gladk)
  NOTE: 20230706: ask for review testing https://lists.debian.org/debian-lts/2023/07/msg00013.html
  NOTE: 20230801: rouca and santiago testing the swarm overlay network (including current buster version)
  NOTE: 20240213: CVE-2024-24557 patch does not directly apply and lack of reproducer test case
  NOTE: 20240310: Dropped from dla-needed.txt (ola/front-desk)
  NOTE: 20230311: Reverted decision to remove from this file since three CVEs are in bullseye. (ola)
  NOTE: 20240407: Version 18.09.1+dfsg1-7.1+deb10u4 in Git has not been uploaded yet. (dleidert)
--
dogecoin
  NOTE: 20230619: Added by Front-Desk (Beuc)
  NOTE: 20230619: CVE-2021-37491 and CVE-2023-30769 seem forgotten by upstream,
  NOTE: 20230619: I suggest pinging/coordinating with upstream to know the current status;
  NOTE: 20230619: also I just referenced 3 older bitcoin-related CVEs to fix;
  NOTE: 20230619: dogecoin not present in bullseye/bookworm, so we lead the initiatives. (Beuc/front-desk)
--
edk2
  NOTE: 20231230: Added by Front-Desk (lamby)
  NOTE: 20231230: CVE-2019-11098 fixed via bullseye 11.2 (lamby)
  NOTE: 20240312: CVE-2023-48733 fixed via DSA-5624-1 (Beuc/front-desk)
--
firmware-nonfree
  NOTE: 20240502: Added by Front-Desk (Beuc)
--
freeimage
  NOTE: 20240320: Added by Front-Desk (ta)
  NOTE: 20240320: lots of postponed issue could be fixed as well
  NOTE: 20240325: Lack of upstream activity,
  NOTE: 20240325: postponed issues are "Revisit when fixed upstream (bunk)
  NOTE: 20240410: See discussion at: https://lists.debian.org/debian-lts/2024/04/threads.html#00012
  NOTE: 20240411: Added some postpone tags for DoS class and removed some where
  NOTE: 20240411: patch is available and has arbitrary code exec class. (ola)
  NOTE: 20240412: ELTS also have a need to update this package.
  NOTE: 20240412: We should open upstream bug reports and push fixes. See above email discussion. (ola)
--
ghostscript (Markus Koschany)
  NOTE: 20240510: Added by Front-Desk (ta)
--
git (Sean Whitton)
  NOTE: 20240519: Added by Front-Desk (utkarsh)
  NOTE: 20240519: there are other no-dsa/postponed issues as well, please batch
  NOTE: 20240519: them, too. Newer ones are RCE and have high severity. (utkarsh)
--
glibc (Adrian Bunk)
  NOTE: 20240504: Re-add for remaining CVEs. (bunk)
  NOTE: 20240520: Testing fixes. (bunk)
  NOTE: 20240603: Testing fixes. (bunk)
--
h2o
  NOTE: 20231228: Added by Front-Desk (lamby)
--
hdf5
  NOTE: 20240511: Added by Front-Desk (ta)
--
i2p
  NOTE: 20230809: Added by Front-Desk (Beuc)
  NOTE: 20230809: Experimental issue-based workflow: please self-assign and follow https://salsa.debian.org/lts-team/lts-updates-tasks/-/issues/28
--
jenkins-htmlunit-core-js
  NOTE: 20231231: Added by Front-Desk (lamby)
  NOTE: 20231231: Needs checking that this is definitely vulnerable: a quick glance
  NOTE: 20231231: … suggests that the embedded copy of htmlunit is very old and may
  NOTE: 20231231: … not even support XLST processing. However, it does use the
  NOTE: 20231231: … TransformerFactory without setting the ~secure flag, so it may
  NOTE: 20231231: … indeed be vulnerable. (lamby)
--
libmojolicious-perl
  NOTE: 20240421: Added by Front-Desk (apo)
--
libreswan
  NOTE: 20230817: Added by Front-Desk (ta)
  NOTE: 20230909: Prepared a patch for CVE-2023-38712 and pushed it to
  NOTE: 20230909: https://salsa.debian.org/lts-team/packages/libreswan.git on the experimental
  NOTE: 20230909: branch. Upstream patch for CVE-2023-38710 does not apply at
  NOTE: 20230909: all due to code refactoring. I intend to package the version
  NOTE: 20230909: from Bullseye instead as soon as the maintainer uploads the fix. (apo)
--
libssh
  NOTE: 20231219: Added by Front-Desk (ta)
  NOTE: 20240225: Patches backported, tests pass.  Backports needs review.
  NOTE: 20240225: Re CVE-2023-48795: untested that Terrapin is actually
  NOTE: 20240225: mitigated.  Upstream have provided some input on doing that:
  NOTE: 20240225: <https://archive.libssh.org/libssh/2024-01/0000000.html>
  NOTE: 20240225: (spwhitton).
  NOTE: 20240227: Re CVE-2023-6918: commit 3eb99562 is simply to fix
  NOTE: 20240227: the build.  It is currently unknown whether it is safe.
  NOTE: 20240227: Upstream have provided some feedback on the issue:
  NOTE: 20240227: <https://archive.libssh.org/libssh/2024-02/0000009.html>
  NOTE: 20240227: (spwhitton).
--
libstb
  NOTE: 20231029: Added by Front-Desk (gladk)
  NOTE: 20231029: A lot of open CVEs. Maybe duplicates.
  NOTE: 20231029: If you take a package, please evaluate it as well as its importance.
  NOTE: 20231119: None of the new CVE fixes has been reviewed by upstream so far,
  NOTE: 20231119: and in the past CVE fixes have caused regressions.
  NOTE: 20231119: Wait for upstream merge of fixes (and fixing in unstable). (bunk)
  NOTE: 20230314: Reverted decision to remove from this file since
  NOTE: 20240314: several CVEs fixed in DLA-3305-1 remain unfixed (no-dsa) in bullseye
  NOTE: 20240314: and bookwork. Uploads to spu and ospu should be coordinated. (roberto)
--
linux (Ben Hutchings)
  NOTE: 20230111: perma-added for LTS package-specific delegation (bwh)
--
linux-5.10
  NOTE: 20231005: perma-added for LTS package-specific delegation (bwh)
--
netty (Markus Koschany)
  NOTE: 20240511: Added by (apo)
--
nodejs (guilhem)
  NOTE: 20240406: Added by Front-Desk (lamby)
--
nova
  NOTE: 20230302: Re-add, request by maintainer (Beuc)
  NOTE: 20230302: zigo says that DLA 3302-1 ships a buster-specific CVE-2022-47951 backport that introduces regression
  NOTE: 20230302: (it's meant to check whether a VMDK image has the "monoliticFlat" subtype, but in practice it breaks compute nodes);
  NOTE: 20230302: cf. debian/patches/cve-2022-47951-nova-stable-rocky.patch, which depends on images_*.patch.
  NOTE: 20230302: "The upstream patch introduces a whitelist of allowed subtype (with monoliticFlat disabled by default).
  NOTE: 20230302:  Though in the Buster codebase, there was no infrastructure to check for this subtype ..." (zigo)
  NOTE: 20230302: Later suites (e.g. bullseye) ship a direct upstream patch and are not affected.
  NOTE: 20230302: We can either rework the patch, or disable .vmdk support entirely.
  NOTE: 20230302: zigo currently has no time and requests the LTS team to do it (IRC #debian-lts 2023-03-02). (Beuc/front-desk)
  NOTE: 20230525: NB. CVE-2023-2088 filed against python-glance-store, python-os-brick, nova and cinder. (lamby)
--
nss
  NOTE: 20240121: Added by Front-Desk (apo)
  NOTE: 20240310: CVE-2023-6135: Upstream suggests to wait until they have a patch for 3.90.x (their LTS version) available and backport from there.
  NOTE: 20240310: see also: Message-ID: <Zd5GYmuVVIDU54Vv@isildor2.loewenhoehle.ip>
  NOTE: 20240310: (2024-02-27 [deblts-team@freexian.com] Re: Current status: nss for buster) (tobi)
--
nvidia-cuda-toolkit
  NOTE: 20230514: Added by Front-Desk (utkarsh)
  NOTE: 20230514: package listed in packages-to-support; a bunch of CVEs have
  NOTE: 20230514: piled up. (utkarsh)
  NOTE: 20230610: Details: https://lists.debian.org/debian-lts/2023/06/msg00032.html
  NOTE: 20230610: my recommendation would be to put the package on the "not-supported" list. (tobi)
  NOTE: 20240311: CVE-2020-5991 is fixed in bullseye. However email sent to suggest removal of support. (ola)
--
nvidia-graphics-drivers
  NOTE: 20240303: Added by Front-Desk (apo)
  NOTE: 20240303: Do we still support the NVIDIA drivers? Can we upgrade to a new upstream release?
  NOTE: 20240303: Maybe it's time to mark them EOL? (apo/front-desk)
--
nvidia-graphics-drivers-legacy-390xx
  NOTE: 20240303: Added by Front-Desk (apo)
  NOTE: 20240303: See comment for nvidia-graphics-drivers. (apo/front-desk)
--
pdns-recursor
  NOTE: 20240306: Added by Front-Desk (opal)
  NOTE: 20240319: Upload postponed due to #1067124 (dleidert)
--
putty
  NOTE: 20231224: Added by Front-Desk (ta)
  NOTE: 20240104: massive code change against bullseye. May be better to backport bullseye (rouca)
  NOTE: 20240324: Backport is straighforward (rouca)
  NOTE: 20240324: https://salsa.debian.org/lts-team/lts-updates-tasks/-/issues/104
  NOTE: 20240412: Wait for comments by maintainer
  NOTE: 20240430: Backport fixes for  CVE-2024-31497 wait review
--
pypy3
  NOTE: 20240503: Added by Front-Desk (Beuc)
  NOTE: 20240503: Fix newly triaged (but old) issues;
  NOTE: 20240503: follow PU #1070218;
  NOTE: 20240503: check with maintainers about syncing bullseye too (Beuc/front-desk)
--
python-asyncssh
  NOTE: 20240116: Added by Front-Desk (lamby)
  NOTE: 20240131: Patch for CVE-2023-46445 and CVE-2023-46446 backported and in Git, but one test is failing. Waiting for feedback before release. (dleidert)
--
rails
  NOTE: 20220909: Re-added due to regression (abhijith)
  NOTE: 20220909: Regression on 2:5.2.2.1+dfsg-1+deb10u4 (abhijith)
  NOTE: 20220909: Two issues https://lists.debian.org/debian-lts/2022/09/msg00014.html (abhijith)
  NOTE: 20220909: https://lists.debian.org/debian-lts/2022/09/msg00004.html (abhijith)
  NOTE: 20220909: upstream report https://github.com/rails/rails/issues/45590 (abhijith)
  NOTE: 20220915: 2:5.2.2.1+dfsg-1+deb10u5 uploaded without the regression causing patch (abhijith)
  NOTE: 20220915: Utkarsh prepared a patch and is on testing (abhijith)
  NOTE: 20221003: https://github.com/rails/rails/issues/45590#issuecomment-1249123907 (abhijith)
  NOTE: 20221024: Delay upload, see above comment, users have done workaround. Not a good idea
  NOTE: 20221024: to break thrice in less than 2 month.
  NOTE: 20230131: Utkarsh to start a thread with sec+ruby team with the possible path forward. (utkarsh)
  NOTE: 20230828: want to rollout ruby-rack first. (utkarsh)
--
ring
  NOTE: 20230903: Added by Front-Desk (gladk)
  NOTE: 20230928: will be likely hard to fix see https://lists.debian.org/debian-lts/2023/09/msg00035.html (rouca)
--
roundcube (guilhem)
  NOTE: 20240524: Added by Front-Desk (lamby)
--
ruby2.5 (utkarsh)
  NOTE: 20240504: Added by Front-Desk (Beuc)
  NOTE: 20240504: Follow DSA-5677-1 (Beuc/front-desk)
  NOTE: 20240528: have working patches ready, will need extensive testing. (utkarsh)
--
runc (dleidert)
  NOTE: 20240312: Added by coordinator (roberto)
  NOTE: 20240314: Several CVEs fixed in LTS remain unfixed (no-dsa) in bullseye.
  NOTE: 20240314: Uploads to ospu should be coordinated. (roberto)
  NOTE: 20240521: Already started to work on it. Upload will haben until end of month. (dleidert)
  NOTE: 20240531: Waiting for ok to upload to bullseye-pu <https://bugs.debian.org/1072248> (dleidert)
--
sendmail (rouca)
  NOTE: 20231224: Added by Front-Desk (ta)
  NOTE: 20240213: Patch need to be extracted (rouca). Upstream does not publish patches (CVE-2023-51765)
  NOTE: 20240217: Patch extracted and being reviewed (rouca)
  NOTE: 20240310: Dropped from dla-needed.txt (ola/front-desk)
  NOTE: 20240311: Re-added to dla-needed.txt; while secteam tagged it no-dsa in later dists,
  NOTE: 20240311: I believe we should fix this sponsored package, like postfix and exim, in all dists,
  NOTE: 20240311: please coordinate with the package maintainer to help make this happen. (Beuc/front-desk)
  NOTE: 20240324: some issue coordinate with myself and security team (rouca)
  NOTE: 20240425: need more time to investigate issue
  NOTE: 20240430: https://marc.info/?l=oss-security&m=171447187004229&w=2
  NOTE: 20240506: add possible workarround see #1070190
  NOTE: 20240514: sid is on the way
  NOTE: 20240525: sid/bookworm ok. Bullseye PU
--
squid
  NOTE: 20240109: Added by Front-Desk (apo)
  NOTE: 20240109: I ask for another pair of eyes for CVE-2023-5824. The fix
  NOTE: 20240109: appears to be intrusive. I could not locate the fix for CVE-2023-49288 yet. (apo)
  NOTE: 20240430: Patch for CVE-2023-49288 has been located and added to tracker (dleidert)
--
suricata (Adrian Bunk)
  NOTE: 20230620: Added by Front-Desk (Beuc)
  NOTE: 20230620: 15+ CVEs marked no-dsa; since the package is supported, with last LTS update in Jessie,
  NOTE: 20230620: I'd suggest reviewing the CVEs, precise the triage (postponed/ignored),
  NOTE: 20230620: and possibly issue a DSA with a few CVEs that were fixed in later dists (Beuc/front-desk)
  NOTE: 20230714: Still reviewing+testing CVEs. (bunk)
  NOTE: 20230731: Still reviewing+testing CVEs. (bunk)
  NOTE: 20231016: Still reviewing+testing CVEs. (bunk)
  NOTE: 20231120: DLA coming soon. (bunk)
--
tiff (Thorsten Alteholz)
  NOTE: 20240314: Added by coordinator (roberto)
  NOTE: 20240314: Several CVEs fixed in LTS remain unfixed (no-dsa) in bullseye and
  NOTE: 20240314: bookworm. Uploads to spu and ospu should be coordinated. (roberto)
  NOTE: 20240526: not satisfied with results, so still testing package
--
tinymce
  NOTE: 20231123: Added by Front-Desk (ola)
  NOTE: 20231216: Someone with more XSS experience needed to assess the
  NOTE: 20231216: severity of CVE-2023-48219.  Also not clear to me that
  NOTE: 20231216: upstream's patch is backportable, as the code has changed a
  NOTE: 20231216: lot.  (spwhitton)
--
tryton-server
  NOTE: 20240421: Added by Front-Desk (apo)
  NOTE: 20240421: Fix causes regressions in tryton client. Waiting for that
  NOTE: 20240421: being resolved upstream.
--
varnish
  NOTE: 20231117: Added by Front-Desk (apo)
  NOTE: 20231204: Working on pre commits for CVE-2023-44487, https://github.com/varnishcache/varnish-cache/pull/4004
  NOTE: 20231219: Continuing work
  NOTE: 20240108: Backported security fixes and related commits. Fixing test failures. (abhijith)
  NOTE: 20240122: Still fixing tests (abhijith)
  NOTE: 20240213: Fixing tests.(abhijith)
--
zookeeper
  NOTE: 20240324: Added by Front-Desk (ta)
  NOTE: 20240502: Persistent (and p-recursive) watches were introduced by ZOOKEEPER-1416, which only exists in 3.6+.
  NOTE: 20240502: See https://issues.apache.org/jira/browse/ZOOKEEPER-1416
  NOTE: 20240502: However, classical watches are used (<< 3.6), it seems that to trigger for nodes whose names are not
  NOTE: 20240502: known in advance is not possible. Nevertheless classical watch leaks some information.
  NOTE: 20240502: CVE-2024-23944 may be therefore downgraded for << 3.6
--
